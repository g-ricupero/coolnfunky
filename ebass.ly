\include "include/global.ly"
\include "include/outline.ly"
\include "include/rhythm.ly"
\include "include/ebass.ly"
\include "include/harmony.ly"

\header {
	instrument = \markup \italic \normalsize {
		\override #'(font-name . "Arial")
		"Bass"
	}
}

\paper {
	#(set-paper-size "a4")          % paper size
	% ragged-last-bottom = ##f      % fill the page till bottom
	% page-count = 1                % force to render just in 1 page
	% between-system-padding = 0    % empty padding between lines
}

\score {
	<<
		% \new ChordNames {
		% 	\set chordChanges = ##t
		% 	\harmony
		% }
		\transpose c c' \new Staff <<
			\clef bass
			\WithChords \global \rhythm \ebass
		>>
		% \transpose c c' \new Staff <<
		% 	\clef bass
		% 	\NoChords \global \ebass
		% >>
	>>
	\layout {}
}
