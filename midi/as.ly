\include "articulate.ly"
\include "../include/global.ly"
\include "../include/harmony.ly"
\include "../include/outline.ly"
\include "../include/alto.ly"

\score {
	\unfoldRepeats \articulate
	\new StaffGroup <<
		\new ChordNames {
			\harmony
		}
		\new Staff <<
			\set Staff.midiInstrument = "alto sax"
			\global \outline \alto
		>>
	>>
	\midi {}
}
